The Mediocre Programmer Presentation for Penguicon 2019

This presentation is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][1].

[1]: http://creativecommons.org/licenses/by-sa/4.0
